local Plugin = { "folke/which-key.nvim" }

Plugin.event = "VeryLazy"
Plugin.init = function()
	vim.o.timeout = true
	vim.o.timeoutlen = 300
end

return Plugin
