local Plugin = { "AckslD/nvim-neoclip.lua" }

Plugin.requires = {
	{'nvim-telescope/telescope.nvim'},
}
Plugin.config = function()
	require('neoclip').setup()
end

return Plugin
