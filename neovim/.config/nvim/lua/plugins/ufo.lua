local Plugin = {"kevinhwang91/nvim-ufo", opts = {}}

Plugin.dependencies = { 'kevinhwang91/promise-async' }
Plugin.config = function()
	vim.o.foldcolumn = '1' -- '0' is not bad
	vim.o.foldlevel = 909 -- Using ufo provider need a large value, feel free to decrease the value
	vim.o.foldlevelstart = 990
	vim.o.foldenable = true

	require('ufo').setup({
		provider_selector = function(bufnr, filetype, buftype)
			return {'treesitter', 'indent'}
		end
	})
end

return Plugin
