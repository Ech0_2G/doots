local Plugin = { 'nvimdev/dashboard-nvim' }

Plugin.dependencies = { {'nvim-tree/nvim-web-devicons'}}
Plugin.event = {'VimEnter'}
Plugin.opts = function(_, opts)
	local logo = [[

	____ ___ _  _ ____ ____ ____ _       ____ ____ _  _ ____ ____ ____ 
	|___  |  |__| |___ |__/ |__| |       |___ |    |__| |  | |___ [__  
	|___  |  |  | |___ |  \ |  | |___    |___ |___ |  | |__| |___ ___] 


		]]

	logo = string.rep("\n", 8) .. logo .. "\n\n"

	local options = {
		config = {
			header = vim.split(logo, "\n"),
		}
	}

	return options
end

return Plugin
