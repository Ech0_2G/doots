local Plugin = {'mfussenegger/nvim-dap'}

Plugin.dependencies = {
	"rcarriga/nvim-dap-ui"
}

Plugin.config = function()
	local dap = require("dap")

	dap.adapters.godot = {
		type = "server",
		host = '127.0.0.1',
		port = 6006,
	}

	dap.configurations.gdscript = {
		{
			type = "godot",
			request = "launch",
			name = "Launch scene",
			project = "${workspaceFolder}",
		}
	}
end

return Plugin
