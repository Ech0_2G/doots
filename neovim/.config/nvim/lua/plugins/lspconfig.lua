local Plugin = {"neovim/nvim-lspconfig"}

Plugin.config = function()
	local lspconfig = require('lspconfig')
	local css_capabilities = vim.lsp.protocol.make_client_capabilities()
	local cmp_capabilities = require('cmp_nvim_lsp').default_capabilities()
	local wp = require('wordpress')

	css_capabilities.textDocument.completion.completionItem.snippetSupport = true

	lspconfig.elixirls.setup {
		cmd = { "/home/caldwellysr/packages/elixir-ls-v0.20.0/language_server.sh" },
		capabilities = cmp_capabilities
	}

	lspconfig.intelephense.setup(wp.intelephense)

	lspconfig.gdscript.setup{}

	lspconfig.bashls.setup{}

	lspconfig.cssls.setup {
		capabilities = css_capabilities,
	}

	lspconfig.html.setup{}

	lspconfig.clangd.setup{}

	lspconfig.ts_ls.setup{}

	lspconfig.pylsp.setup{
		settings = {
			pylsp = {
				plugins = {
					pycodestyle = {
						ignore = {'W391'},
						maxLineLength = 100
					}
				}
			}
		}
	}

	lspconfig.rust_analyzer.setup{
		settings = {
			['rust-analyzer'] = {
				diagnostics = {
					enable = false;
				}
			}
		}
	}

	vim.keymap.set('n', '<Leader>e', vim.diagnostic.open_float)
	vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
	vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
	vim.keymap.set('n', '<Leader>dq', vim.diagnostic.setloclist)

	vim.api.nvim_create_autocmd('LspAttach', {
		group = vim.api.nvim_create_augroup('UserLspConfig', {}),
		callback = function(ev)
			vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

			local opts = { buffer = ev.buf }
			vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
			vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
			vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
			vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
			vim.keymap.set('n', '<M-K>', vim.lsp.buf.signature_help, opts)
			vim.keymap.set('n', '<Leader>wa', vim.lsp.buf.add_workspace_folder, opts)
			vim.keymap.set('n', '<Leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
			vim.keymap.set('n', '<Leader>wl', function()
				print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
			end, opts)
			vim.keymap.set('n', '<Leader>D', vim.lsp.buf.type_definition, opts)
			vim.keymap.set('n', '<Leader>rn', vim.lsp.buf.rename, opts)
			vim.keymap.set({ 'n', 'v' }, '<Leader>ca', vim.lsp.buf.code_action, opts)
			vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
			vim.keymap.set('n', '<Leader>fo', function()
				vim.lsp.buf.format { async = true }
			end, opts)

			vim.api.nvim_create_autocmd('BufWritePre', {
				buffer = ev.buf,
				callback = function()
					vim.lsp.buf.format({
						async = false,
						timeout_ms = 3000
					})
				end
			})
		end
	})
end

return Plugin
