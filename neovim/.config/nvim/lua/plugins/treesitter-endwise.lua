local Plugin = {"RRethy/nvim-treesitter-endwise"}

Plugin.config = function() 
	require('nvim-treesitter.configs').setup {
		endwise = {
			enable = true,
		}
	}
end

return Plugin
