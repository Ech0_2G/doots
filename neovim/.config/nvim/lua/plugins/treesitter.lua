local Plugin = {"nvim-treesitter/nvim-treesitter"}

Plugin.build = ":TSUpdate"

Plugin.config = function()
	require("nvim-treesitter.configs").setup({
		ensure_installed = {
			"bash",
			"css",
			"dockerfile",
			"eex",
			"elixir",
			"gdscript",
			"git_config",
			"git_rebase",
			"gitattributes",
			"gitcommit",
			"gitignore",
			"godot_resource",
			"heex",
			"html",
			"javascript",
			"json",
			"lua",
			"markdown",
			"markdown_inline",
			"muttrc",
			"query",
			"ruby",
			"rust",
			"sql",
			"ssh_config",
			"tmux",
			"typescript",
			"vim",
			"vimdoc",
			"xml",
			"yaml",
		},
		sync_install = false,
		highlight = { enable = true },
		indent = { enable = true },
	})
end

return Plugin
