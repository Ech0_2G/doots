vim.keymap.set("n", "<leader>w", ":w<CR>", {desc = "Write Buffer"})
vim.keymap.set("n", "<leader>W", ":w ! sudo tee %<CR>", {desc = "Seriously write buffer", nowait = true})
vim.keymap.set("n", "<leader>q", ":bd<CR>", {desc = "Close Buffer"})
vim.keymap.set("i", "jj", "<Esc>", {desc = "Exit insert mode"})

vim.keymap.set("n", "<Left>", ":bprev<CR>", {desc = "Previous Buffer"})
vim.keymap.set("n", "<Right>", ":bnext<CR>", {desc = "Next Buffer"})

vim.keymap.set("n", "<leader>c", ":e ~/.config/nvim/init.lua<CR>", opts)
vim.keymap.set("n", "<leader>l", ":e ~/.config/nvim/lua/", opts)
vim.keymap.set("n", "<leader>so", ":source ~/.config/nvim/init.lua<CR>", opts)
vim.keymap.set("n", "<leader>G", ":Neotree git_status<CR>", {desc = "Git status"})

vim.keymap.set("n", "<C-j>", "<C-w>j", opts)
vim.keymap.set("n", "<C-k>", "<C-w>k", opts)
vim.keymap.set("n", "<C-h>", "<C-w>h", opts)
vim.keymap.set("n", "<C-l>", "<C-w>l", opts)

vim.keymap.set("n", "<C-n>", ":Neotree toggle<CR>", {desc = "Show NeoTree File Explorer"})

vim.keymap.set("n", "<leader>t", ":%s/\\s\\+$//e<CR>", opts)

vim.keymap.set("v", "<leader>s", ":sort u<CR>", {desc = "Sort Selection"})
