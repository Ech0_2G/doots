vim.g.mapleader = " "

require("lazy.init")
require("user.autocmd")
require("user.settings")
require("user.keymaps")
require("user.server-start")
