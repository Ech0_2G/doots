![Screenshot](screenshot.png)

#Requirements
- git `pacman -S git`
- [stow](https://www.gnu.org/software/stow/) `pacman -S stow`

-----

```
pacman -S git stow
git clone https://gitlab.com/Ech0_2G/doots.git
cd doots
make
```
